import java.util.Scanner;

public class InputVal extends CheckingTheInputDate {
    public InputVal() {
    }

    public void scan() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Введи выражение:");
            String example = sc.nextLine();
            if (example.equals("exit")) {
                break;
            } else

                CheckingTheInputDate.check(example);
        }
    }
}