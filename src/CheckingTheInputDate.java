import java.util.InputMismatchException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CheckingTheInputDate {
    public static void check(String str1) {

        Pattern p = Pattern.compile("[^*+./^)(_0-9-]");
        Matcher m = p.matcher(str1);

        if (m.find()) {
            throw new InputMismatchException("Ввод не допустимого символа");
        } else {
            CheckingTheInputDate.addZero(str1);
        }
    }


    public static void addZero(String qwerty) {
        String zero = new String();
        for (int add = 0; add < qwerty.length(); add++) {
            char symbol = qwerty.charAt(add);
            if (symbol == '-') {
                if (add == 0) {
                    zero += '0';
                } else if (qwerty.charAt(add - 1) == '(') {
                    zero += '0';
                }

            }
            zero += symbol;
        }
        Program.inOut(zero);
    }

}
