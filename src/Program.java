import java.util.*;

public class Program extends CheckingTheInputDate {

    public static void inOut(String example) {
        String currect = "";
        Stack<Character> save = new Stack<>();
        int prior;

        for (int i = 0; i < example.length(); i++) {
            prior = priority(example.charAt(i));
            if (prior == 4) {
                currect += example.charAt(i);
            } else if (prior == 2) {
                save.push(example.charAt(i));
            } else if (prior == 1 || prior == 0 || prior == -1) {
                currect += ' ';
                while (!save.empty()) {
                    if (priority(save.peek()) <= prior) {
                        currect += save.pop();
                    } else break;
                }
                save.push(example.charAt(i));
            } else if (prior == 3) {
                currect += ' ';
                while (priority(save.peek()) != 2) {
                    currect += save.pop();
                }
                save.pop();
            }
        }

        while (!save.empty()) {
            currect += save.pop();
        }
        out(currect);
    }


    public static void out(String formatExample) {
        String operand = new String();
        Stack<Double> saveD = new Stack<>();

        for (int i = 0; i < formatExample.length(); i++) {
            if (formatExample.charAt(i) == ' ') {
                continue;
            } else if (priority(formatExample.charAt(i)) == 4) {
                while (formatExample.charAt(i) != ' ' && priority(formatExample.charAt(i)) == 4) {
                    operand += formatExample.charAt(i++);
                    if (i == formatExample.length()) {
                        break;
                    }
                }

                saveD.push(Double.parseDouble(operand));
                operand = new String();
            }
            if (priority(formatExample.charAt(i)) == -1) {
                double x = saveD.pop();
                saveD.push(x * x);
            }

            if (priority(formatExample.charAt(i)) == 1 || priority(formatExample.charAt(i)) == 0) {
                double a = saveD.pop();
                double b = saveD.pop();
                if (formatExample.charAt(i) == '+') {
                    saveD.push(b + a);
                } else if (formatExample.charAt(i) == '-') {
                    saveD.push(b - a);
                } else if (formatExample.charAt(i) == '*') {
                    saveD.push(b * a);
                } else if (formatExample.charAt(i) == '/') {
                    if (a == 0) {
                        throw new ArithmeticException("Произошло деление на ноль!");
                    } else {
                        double c = b / a;
                        saveD.push(c);
                    }
                }
            }
        }
        System.out.println("Выражение равно: " + saveD.pop());
        System.out.println("Чтобы выйти из приложения введите: exit");
    }


    private static int priority(char symbol) {
        if (symbol == '*' || symbol == '/') {
            return 0;
        } else if (symbol == '+' || symbol == '-') {
            return 1;
        } else if (symbol == '(') {
            return 2;
        } else if (symbol == ')') {
            return 3;
        } else if (symbol == '^') {
            return -1;
        } else {
            return 4;
        }
    }

}
